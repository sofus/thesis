package dk.itu.tracy.jenkins;

import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import dk.itu.tracy.entity.Entity;
import dk.itu.tracy.facde.AMQPFacade;
import dk.itu.tracy.parser.Parser;


/**
 * first param: properties file for AMQP bridge second param: JSON file
 */
public class JenkinsParser implements Parser{
	public static void main(String[] args) throws IOException {
		if (args.length < 2) {
			System.out.println("Error: need 2 arguments");
			System.exit(2);
		}
		FileReader fr = new FileReader(args[0]);
		Properties prop = new Properties();
		prop.load(fr);
		AMQPFacade facade = new AMQPFacade(prop);
		String jsonString = args[1];
		JenkinsParser jp = new JenkinsParser();
		List<Entity> ents = jp.parse(jsonString);
		System.out.println("msg parsed");
		for (Entity entity : ents) {
			facade.sendEvent(entity);
		}
		System.out.println("msg sent");
		System.exit(0);
	}

	public List<Entity> parse(String jsonString) {
		Entity ent = new Entity();
		List<Entity> out = new ArrayList<>();
		JsonObject json = new JsonParser().parse(jsonString).getAsJsonObject();
		long date = json.get("timestamp").getAsLong();
		ent.setTimestamp(new Date(date));
		ent.setId(json.get("id").getAsString());
		try {
			ent.setUrl(new URI(json.get("url").getAsString()));
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		if(json.get("building").getAsBoolean())
			ent.setType("jenkins-build-started");
		else
			ent.setType("jenkins-build-done");
		ent.setData(json);
		JsonArray ja=json.get("changeSet").getAsJsonObject()
			.get("items").getAsJsonArray();
		String[]prev = new String[ja.size()];
		for (int i = 0; i < prev.length; i++) {
			prev[i]=ja.get(i).getAsJsonObject()
				.get("id").getAsString();
		}
		ent.setPrev(prev);
		out.add(ent);
		return out;
	}
}
