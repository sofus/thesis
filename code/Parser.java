package dk.itu.tracy.parser;

import java.util.List;

import dk.itu.tracy.entity.Entity;

public interface Parser {
        public List<Entity> parse(String inputString);
}
