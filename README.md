Effective traceability in an open source driven software development lifecycle
====================

The need for efficient traceability in a distributed software development lifecycle (SDLC) is growing with the size of the product under construction. 
As the lifecycle gets more and more complex, the traceability between the different artifacts is increasingly hard to obtain efficiently, making creation and alternation of a pipeline difficult. 
By automatically integrating the events of the tools in a graph database and leveraging the ideas of the semantic web, we can make a visualization of the pipeline, giving the much wanted traceability in the SDLC. 
This thesis will present a data format for creating the graph structure, as well as a framework for extracting the relevant information from the tools included in a distributed SDLC.
![](/img/artifactandevents.png "Graph representation of Tracy")

